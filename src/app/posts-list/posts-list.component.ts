import { Component, OnInit, OnDestroy } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';
import { HttpClient } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser';

import { PostsService } from './services/posts.service';
import { Post } from './models/post.model';
import { PostTypeEnum } from './enums/post-type.enum'


@Component({
  selector: 'app-posts-list',
  templateUrl: './posts-list.component.html',
  styleUrls: ['./posts-list.component.css']
})
export class PostsListComponent implements OnInit, OnDestroy {

  posts: Post[] = [];

  notEmptyPost = true;
  notScrolly = true;
  page = 1;
  subsScroll;

  destroy = new Subject<boolean>();

  constructor(private postsService: PostsService, 
              private spinner: NgxSpinnerService,
              private http: HttpClient,
              private domSanitizer: DomSanitizer) {
   }

  ngOnInit(): void {

    this.postsService.getAllPosts().pipe(
      takeUntil(this.destroy)
    ).subscribe((response) => {
      this.posts = response;
      // Load all videos
       /* this.posts.forEach(post => {
         if (post.type === PostTypeEnum.VIDEO) {
             post.url = this.domSanitizer.bypassSecurityTrustResourceUrl(`https://www.youtube.com/embed/${post.url.substring(32)}`);
         }  
       });  */
       this.posts.reverse();
    });
    
    
  }

  ngOnDestroy(): void {
    this.destroy.next(true);
    this.destroy.unsubscribe();
    this.subsScroll.unsubscribe();
  }

  onDeletePost(itemId) {
    this.postsService.deletePost(itemId).subscribe(() => {
      this.posts.splice(this.posts.indexOf(itemId), 1);
      this.postsService.getAllPosts().subscribe((response) => {
        this.posts = response;
        this.posts.reverse();
      });
      alert('The post was deleted successfuly.');
    });
  }

  onScroll() {
    if (this.notScrolly && this.notEmptyPost) {
      this.spinner.show();
      this.notScrolly = false;
      this.loadNextPosts();
    } 
  } 

    loadNextPosts() {
    const lastPost = this.posts[this.posts.length - 1];
    const lastPostID = (lastPost.id).toString();
    const dataUrl = `http://localhost:3000/posts?_page=${this.page}&_limit=5`;
    const dataToSend = new FormData();
    dataToSend.append('id', lastPostID);
    this.subsScroll = this.http.post(dataUrl, dataToSend)
    .subscribe((data: any) => {
      const nextPosts = data[0];
      this.spinner.hide();
      this.page++;

      if(nextPosts.length === 0 ) {
        this.notEmptyPost = false;
      }

      this.posts = this.posts.concat(nextPosts);
      this.notScrolly= true;
    }) 
  } 
}
