import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Post } from '../models/post.model';


@Injectable({
  providedIn: 'root'
})

export class PostsService {

  readonly dataUrl = 'http://localhost:3000/posts';

  constructor(private http: HttpClient) {
  }

   getAllPosts(): Observable<Post[]> {
    return this.http.get<Post[]>(this.dataUrl);
  }

  getById(id: string): Observable<Post> {
    return this.http.get<Post>(`${this.dataUrl}/${id}`);
  }

  createPost(postData: Post): Observable<Post> {
    return this.http.post<Post>(this.dataUrl, postData);
  }

  updatePost(id: string, postData: Partial<Post>): Observable<Post> {
    return this.http.patch<Post>(`${this.dataUrl}/${id}`, postData);
  }

  deletePost(id: string): Observable<undefined> {
    return this.http.delete<undefined>(`${this.dataUrl}/${id}`);
  }
}
