export enum PostTypeEnum {
    VIDEO = 'VIDEO',
    IMAGE = 'IMAGE',
    LINK = 'LINK'
}