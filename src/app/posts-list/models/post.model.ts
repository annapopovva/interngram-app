import { PostTypeEnum } from '../enums/post-type.enum';

export class Post {
    id: number;
    author: string;
    username: string;
    type: PostTypeEnum;
    title: string;
    date: string;
    url: string;
}
