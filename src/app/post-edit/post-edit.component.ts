import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { take } from 'rxjs/operators';

import { Post } from '../posts-list/models/post.model';
import { PostsService } from '../posts-list/services/posts.service';

@Component({
  selector: 'app-post-edit',
  templateUrl: './post-edit.component.html',
  styleUrls: ['./post-edit.component.css']
})
export class PostEditComponent implements OnInit {

  post: Post;
  formGroup: FormGroup;

  constructor(private postsService: PostsService,
              private router: Router,
              private route: ActivatedRoute,
              private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');

    if (id) {
      this.postsService.getById(id).pipe(
        take(1)
      ).subscribe(response => {
        this.post = response;

        this.buildForm();
      });
    } else {
      this.post = new Post();
      this.post.date = (new Date().toDateString());
      this.buildForm();
    }
  }

  onSave(): void {
    if (this.formGroup.invalid) {
      alert('Invalid form. Please enter all the fields correctly!');

      return;
    }
    const id = this.route.snapshot.paramMap.get('id');
    const data = this.formGroup.value as Post;
    if(id) {
      this.postsService.updatePost(id, data).pipe(
        take(1)
      ).subscribe(() => {
        this.router.navigate(['']).then(() => {
          alert('The post was successfuly edited.');
        });
      });
    } else {
      this.postsService.createPost(data).pipe(
        take(1)
      ).subscribe(() => {
        this.router.navigate(['']).then(() => {
          alert('The post was saved.');
        });
      });
    }

  }

  private buildForm(): void {
    this.formGroup = this.formBuilder.group({
      author: [this.post.author, Validators.required],
      username: [this.post.username, Validators.required],
      title: [this.post.title, Validators.required],
      type: [this.post.type, Validators.required],
      url: [this.post.url, [Validators.required]],
      date: [this.post.date]
    });
  }
}
